const http = require('http');

const PORT = 4000;

http.createServer(function(request, response) {

	if (request.url === "/localhost:4000" && request.method === "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	} 

	else if(request.url == "/localhost:4000/profile" && request.method === "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	}


	else if(request.url == "/localhost:4000/courses" && request.method === "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end(`Here's our courses available`)
	}

	else if(request.url == "/localhost:4000/addcourse" && request.method === "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add a course to our resources')
	}

	else if(request.url == "/localhost:4000/updatecourse" && request.method === "PUT"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Update a course to our resources')
	}

	else if(request.url == "/localhost:4000/archivecourses" && request.method === "DELETE" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Archive courses to our resources')
	}

		

}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)